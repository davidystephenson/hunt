from .game import Game


def main():
    game = Game()
    game.close()


if __name__ == '__main__':
    main()
