from hunt.body import Body
from random import randint, uniform


class Actor():
    def __init__(
        self,
        world,
        x=0.0,
        y=0.0,
        color=(255, 255, 255),
        role='actor',
        force=0.0,
        body_type='dynamic',
    ):
        self.world = world
        self.game = self.world.game
        self.force = force
        self.contacts = []
        self.bodies = []
        self.role = role
        self.color = color
        self.center = 0, 0
        self.body = Body(
            actor=self,
            x=x,
            y=y,
            body_type=body_type,
        )
        self.ready = False
        self.world.actors.append(self)

        if self.role in self.world.roles:
            self.world.roles[self.role].append(self)
        else:
            self.world.roles[self.role] = [self]

        self.cls = self.my_class()
        self.age = 0

    @classmethod
    def my_class(cls):
        return cls

    def child_update(self):
        pass

    def child_destroy(self):
        pass

    def destroy(self):
        try:
            self.world.actors.remove(self)
        except ValueError:
            pass

        try:
            self.world.roles[self.role].remove(self)
        except ValueError:
            pass

        for body in self.bodies:
            self.world.b2world.DestroyBody(body.b2body)
        self.bodies = []

        self.child_destroy()

    def draw(self):
        try:
            for body in self.bodies:
                body.draw()
        except TypeError:
            print "Draw error!"
            print "color test: " + str(self.color)

    def handle_contact(self, actor):
        pass

    def handle_contacts(self):
        if self.role == 'mountain':
            self.ready = True
            for contact in self.contacts:
                self.handle_contact(contact[1])
        else:
            if self.ready:
                for contact in self.contacts:
                    self.handle_contact(contact[1])
            else:
                if len(self.contacts):
                    self.relocate()
                else:
                    self.ready = True
        self.contacts = []

    def move(self):
        pass

    def relocate(self):
        self.ready = False
        self.body.b2body.position = self.world.new_point()

    def update(self):
        self.center = (
            self.body.b2body.GetWorldPoint(localPoint=(0, 0))
        )
        self.handle_contacts()
        self.move()
        self.child_update()
        self.age = self.age + 1
        self.draw()

    def wander(self, force=None):
        if not force:
            force = self.force
        center = self.body.b2body.GetWorldPoint(localPoint=(0, 0))
        x_force = uniform(-force, force)
        y_force = uniform(-force, force)
        self.body.b2body.ApplyForce(
            force=(x_force, y_force),
            point=center,
            wake=True,
        )
