from hunt.actor import Actor


class Scenery(Actor):
    def __init__(
        self,
        world,
        x=0.0,
        y=0.0,
        body_type='dynamic',
        force=0.0,
        role='scenery',
        color=(50, 50, 50),
        density=1.0,
    ):
        color = 75 + (1 - density) * 180
        color = (color, color, color)
        Actor.__init__(
            self,
            world,
            x,
            y,
            body_type=body_type,
            force=force,
            role=role,
            color=color,
        )
        self.body.b2body.density = density * 5000
