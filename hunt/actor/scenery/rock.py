from hunt.actor.scenery import Scenery
from hunt.fixture.square import Square


class Rock(Scenery):
    def __init__(
        self,
        world,
        x=0.0,
        y=0.0,
        width=1.0,
        height=1.0,
        density=1.0,
        body_type='dynamic',
        role='rock',
    ):
        Scenery.__init__(
            self,
            world,
            x,
            y,
            density=density,
            body_type=body_type,
            role=role,
        )
        self.square = Square(
            body=self.body,
            width=width,
            height=height,
            color=self.color,
        )
