from hunt.actor import Actor
from hunt.fixture.triangle import Triangle
from random import uniform


class River(Actor):
    def __init__(
        self,
        world,
        x=0.0,
        y=0.0,
        width=1.0,
        height=1.0,
        density=0.1,
        body_type='dynamic',
        role='river'
    ):
        seed = uniform(100, 255)
        base = (width * height) / 2.0
        force = uniform(0, (255/seed) * 100 * base)

        shade = 355 - seed
        color = (shade, shade, shade)

        Actor.__init__(
            self,
            world,
            x,
            y,
            body_type=body_type,
            force=force,
            role=role,
            color=color,
        )
        self.class_name = River
        self.triangle = Triangle(
            body=self.body,
            width=width,
            height=height,
            color=self.color
        )

    def child_update(self):
        self.wander()
