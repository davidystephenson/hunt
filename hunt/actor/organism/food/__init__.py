from hunt.actor.organism import Organism


class Food(Organism):
    def __init__(
        self,
        world,
        x,
        y,
        color=(0, 255, 0),
        role='food',
        force=0.0,
        size=0.5,
        hunger=0.0,
        parent=None,
        fixture=None,
    ):
        Organism.__init__(
            self,
            world,
            x,
            y,
            color=color,
            role=role,
            force=force,
            size=size,
            hunger=hunger,
            parent=parent,
            fixture=fixture,
        )

    def child_handle_contact(self, contact):
        pass

    def handle_contact(self, actor):
        if actor.role in ['predator', 'grazer']:
            self.die()
        self.child_handle_contact(actor)
