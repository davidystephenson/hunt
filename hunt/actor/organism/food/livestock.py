from hunt.actor.organism.food import Food
from hunt.fixture.triangle import Triangle


class Livestock(Food):
    def __init__(
        self,
        world,
        x,
        y,
        color=(0, 255, 0),
        role='livestock',
        force=50.0,
        size=1.0,
        hunger=0.0002,
        parent=None,
        fixture=Triangle,
    ):
        Food.__init__(
            self,
            world,
            x,
            y,
            color=color,
            role=role,
            force=force,
            size=size,
            parent=parent,
            hunger=hunger,
            fixture=fixture,
        )

    def move(self):
        self.wander()

    def child_handle_contact(self, actor):
        if actor.role == 'plant':
            self.hunger_damage -= actor.health * actor.size * 2
