from hunt.actor.organism.food import Food
from hunt.fixture.square import Square
from random import random


class Plant(Food):
    def __init__(
        self,
        world,
        x,
        y,
        color=(0, 255, 0),
        role='plant',
        size=0.5,
        hunger=0.001,
        parent=None,
        fixture=Square,
    ):
        Food.__init__(
            self,
            world,
            x,
            y,
            color=color,
            role=role,
            size=size,
            hunger=hunger,
            parent=parent,
            fixture=fixture,
        )

    def child_handle_contact(self, actor):
        if actor.role == 'livestock':
            self.die()
