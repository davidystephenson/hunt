from hunt.actor.organism import Organism
from hunt.actor.organism.food.plant import Plant
from hunt.actor.waste import Waste
from hunt.controller import Controller
from hunt.fixture.circle import Circle
from random import random
import math

COMBAT = 0.1

class Predator(Organism):
    def __init__(
        self,
        world,
        x,
        y,
        color=(255, 255, 255),
        role='predator',
        force=13.5,
        size=0.5,
        hunger=0.0005,
        parent=None,
        control=False,
        fixture=Circle,
    ):
        if parent:
            mutation = random()
            change = (random() * 2) / 10.0
            increase = 1 + change
            decrease = 1 + change / 2
            if mutation < 0.3:
                force = parent.force * increase
                hunger = parent.hunger * decrease
                size = parent.size / increase
            elif mutation < 0.6:
                force = parent.force / decrease
                hunger = parent.hunger / increase
                size = parent.size / decrease
            elif mutation < 0.9:
                force = parent.force / increase
                hunger = parent.hunger * decrease
                size = parent.size * increase
        Organism.__init__(
            self,
            world,
            x,
            y,
            color=color,
            role=role,
            force=force,
            size=size,
            hunger=hunger,
            parent=parent,
            fixture=fixture,
        )
        self.control = control
        self.controller = Controller(self)

        if self.color in self.world.colors:
            self.world.colors[self.color].append(self)
        else:
            self.world.colors[self.color] = [self]

    # def convert(self):
    # if (actors_length < 275 and colors_length < 10) or colors_length < 5:
    #     new_color = self.world.unique_color()
    #     self.colors[new_color] = self.colors.pop(self.color)
    #     self.color = new_color
    #     self.shell.color = self.color
    #     self.heart.color = self.color
    #     self.hunger_damage = 0.75
    #     self.combat_damage = 0.0
    #     self.health = 0.99
    #     self.relocate()

    #     print "Unleashing... " + status
    #     self.world.unleash()

    def child_destroy(self):
        try:
            allies = self.world.colors[self.color]
            allies.remove(self)

            allies_length = len(allies)

            if not allies_length:
                self.world.colors.pop(self.color)
        except ValueError:
            pass

    def decompose(self):
        self.waste = Waste(self.world, self.center[0], self.center[1])

    def describe(self, value=False):
        print "player color test: " + str(self.world.player.color)
        print "my color test: " + str(self.color)
        if value:
            print "nutrition test: " + str(value)

        try:
            allies = self.world.colors[self.color]
            print "allies test: " + str(allies)
            allies_length = len(allies)
            print "allies_length test: " + str(allies_length)
        except (ValueError, KeyError) as error:
            pass

        print "age test: " + str(self.age)
        print "predators test: " + str(len(self.world.roles['predator']))
        color_string = str(self.color)
        print "victim color: " + color_string
        print "combat damage: " + str(self.combat_damage)
        print "hunger damage: " + str(self.hunger_damage)

        living = []
        for color, list in self.world.colors.iteritems():
            if len(list) > 0:
                living.append(color)

        living_length = len(living)
        living_length_string = str(living_length)
        print "number of remaining colors: " + living_length_string 
        print "remaining colors: " + str(living)

        actors_length = len(self.world.actors)
        actors_string = str(actors_length)
        print "total actors: " + actors_string

        predators_length = len(self.world.roles['predator'])
        predators_string = str(predators_length)
        print "living predators: " + predators_string

    def die(self, forced=False):
        if self.control and forced:
            print "FORCED DEATH"

        self.destroy()

        meat = ((1.0 - self.hunger_damage) * 3.0) - 0.5
        food = 0.0
        while food < meat:
            size = random()
            self.plant = Plant(self.world, self.center[0], self.center[1], size=size)
            food = food + size

        if self.color in self.world.colors:
            if self.control:
                self.game.screen.fill(self.color)

                allies = self.world.colors[self.color]

                ally = allies[0]

                self.world.player = ally
                self.world.player.control = True
        else: 
            print "EXTINCTION: " + str(self.color)
            if self.control:
                self.world.game.screen.fill((255, 255, 255))
                successor = self.world.roles['predator'][0]
                self.world.player = successor
                self.world.player.control = True
            self.world.unleash()

    def handle_contact(self, actor):
        if actor.role == 'plant':
            nutrition = (actor.health * actor.size) / 3

            self.heal(nutrition)
        if actor.role == 'livestock':
            self.heal(0.75 * actor.health)
        if actor.role == 'predator':
            if actor.color != self.color and self.world.pvp:
                damage = self.sizeup(actor)
                pain = self.combat_damage + damage
                possible = 1 - self.hunger_damage
                injury = min(pain, possible)
                self.combat_damage = injury

                self.wander(force=actor.size * 1000)
                actor.wander(force=self.size * 1000)

                if self.control:
                    self.game.screen.fill((255, 0, 0))

    def heal(self, value):
        if self.hunger_damage >= 0:
            self.hunger_damage -= value
        elif self.combat_damage > value:
            self.combat_damage -= value
        else:
            self.reproduce()


    def hunt(self):
        targets = self.brain.nearest()
        for target in targets:
            if target.role in ['plant', 'livestock']:
                self.brain.rush(target, self.force)
                return None
            elif target.role == 'predator':
                self_ratio = target.size / self.size
                self_jaw = self.health / self.sizeup(target)

                target_ratio = self.size / target.size
                target_jaw = target.health / target.sizeup(self)

                if self_jaw > target_jaw:
                    self.brain.rush(target, self.force)
                    return None
                elif target_jaw > self_jaw:
                    self.brain.rush(target, -self.force)
                    return None
        self.wander()

    def move(self):
        if self.control:
            self.controller.control()
        else:
            self.hunt()

    def sizeup(self, actor):
        ratio = actor.size / self.size
        return COMBAT * math.pow(ratio, 5)

