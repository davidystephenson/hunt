from hunt.actor import Actor
from hunt.brain import Brain


class Organism(Actor):
    def __init__(
        self,
        world,
        x,
        y,
        color=(255, 255, 255),
        role='organism',
        force=0.0,
        size=0.5,
        hunger=0,
        parent=None,
        fixture=None,
    ):
        Actor.__init__(
            self,
            world,
            x,
            y,
            color=color,
            role=role,
            force=force,
        )
        self.size = size
        self.parent = parent
        self.hunger = hunger
        self.hunger_damage = 0.5
        self.combat_damage = 0.0
        self.health = 1.0
        self.brain = Brain(world, self)
        self.shell = fixture(
            body=self.body,
            size=self.size,
            color=self.color
        )
        self.heart = fixture(
            body=self.body,
            size=self.size * 0.9,
            color=self.color,
            is_sensor=True,
        )

    def child_update(self):
        self.hunger_damage += self.hunger
        self.health = 1.0 - (self.hunger_damage + self.combat_damage)
        if self.health <= 0.0:
            self.die()
            return

        while self.health >= 1.0:
            self.reproduce()

        self.heart.color = (
            self.color[0] * self.health,
            self.color[1] * self.health,
            self.color[2] * self.health
        )

    def decompose(self):
        pass

    def die(self):
        self.destroy()

    def reproduce(self):
        child_x, child_y = self.center
        child = self.cls(
            self.world,
            child_x,
            child_y,
            color=self.color,
            parent=self,
        )
        child.ready = True
        self.hunger_damage += 0.5
        self.health = 1.0 - (self.hunger_damage + self.combat_damage)
