from hunt.actor import Actor
from hunt.fixture.square import Square
# from hunt.fixture.circle import Circle


class Mountain(Actor):
    def __init__(
        self,
        world,
        x,
        y,
        width,
        height,
        role='mountain',
        color=(40, 40, 40)
    ):
        Actor.__init__(
            self,
            world,
            x,
            y,
            role=role,
            body_type='static',
            color=color,
        )
        self.border = 0.5
        self.square = Square(
            body=self.body,
            width=width,
            height=height,
            color=self.color,
        )
        # self.top_left = Circle(
        #     body=self.body,
        #     size=self.border,
        #     color=self.color,
        #     x=-(width / 2),
        #     y=-(height / 2),
        # )
        # self.top_right = Circle(
        #     body=self.body,
        #     size=self.border,
        #     color=self.color,
        #     x=width / 2,
        #     y=-(height / 2),
        # )
        # self.bottom_left = Circle(
        #     body=self.body,
        #     size=self.border,
        #     color=self.color,
        #     x=-(width / 2),
        #     y=height / 2,
        # )
        # self.bottom_right = Circle(
        #     body=self.body,
        #     size=self.border,
        #     color=self.color,
        #     x=width / 2,
        #     y=height / 2,
        # )
        # self.top = Square(
        #     body=self.body,
        #     width=width,
        #     height=self.border * 2,
        #     color=self.color,
        #     y=height / 2,
        # )
        # self.bottom = Square(
        #     body=self.body,
        #     width=width,
        #     height=self.border * 2,
        #     color=self.color,
        #     y=-(height / 2),
        # )
        # self.left = Square(
        #     body=self.body,
        #     width=self.border * 2,
        #     height=height,
        #     color=self.color,
        #     x=-(width / 2),
        # )
        # self.right = Square(
        #     body=self.body,
        #     width=self.border * 2,
        #     height=height,
        #     color=self.color,
        #     x=(width / 2),
        # )
