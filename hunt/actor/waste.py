from hunt.actor import Actor
from hunt.fixture.square import Square


class Waste(Actor):
    def __init__(
        self,
        world,
        x=0.0,
        y=0.0,
        size=0.5,
        color=(255, 88, 0),
        role='waste',
    ):
        Actor.__init__(
            self,
            world,
            x,
            y,
            role=role,
            color=color,
        )
        self.square = Square(
            body=self.body,
            size=size,
            color=self.color
        )

    def handle_contact(self, actor):
        if actor.role in ['plant', 'grazer', 'player', 'carnivore']:
            self.die()
