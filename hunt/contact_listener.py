from Box2D import b2ContactListener


class ContactListener(b2ContactListener):
    def __init__(self):
        b2ContactListener.__init__(self)

    def BeginContact(self, contact):
        fixtureA = contact.fixtureA.userData
        fixtureB = contact.fixtureB.userData
        actorA = fixtureA.actor
        actorB = fixtureB.actor
        actorA.contacts.append((actorA, actorB))
        actorB.contacts.append((actorB, actorA))
