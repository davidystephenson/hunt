from hunt.world import World
from hunt.actor.organism.food.plant import Plant
from hunt.actor.organism.food.livestock import Livestock
from hunt.species import Species


class Test(World):
    def __init__(self, game, width=50.0, height=50.0):
        World.__init__(self, game, width, height)

        self.species = []
        self.species.append(Species(self, control=True))
        self.player = self.species[0].predators[0]
        self.species.append(Species(self))

        self.add_actor(Plant, magnitude=10)
        self.add_actor(Livestock, magnitude=10)
