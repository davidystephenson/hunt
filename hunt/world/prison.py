from hunt.world import World
from hunt.actor.mountain import Mountain
from hunt.actor.food.plant import Plant
from hunt.actor.predator.carnivore import Carnivore
from hunt.actor.predator.grazer import Grazer
from hunt.actor.predator.player import Player


class Prison(World):
    def __init__(self, game, width=100.0, height=62.0):
        World.__init__(self, game, width, height)

        # self.left = Mountain(
        #     world=self,
        #     x=25,
        #     y=25.0,
        #     width=1.0,
        #     height=10.0,
        # )
        # self.right = Mountain(
        #     world=self,
        #     x=35.0,
        #     y=25.0,
        #     width=1.0,
        #     height=10,
        # )
        # self.top = Mountain(
        #     world=self,
        #     x=30,
        #     y=20,
        #     width=10,
        #     height=1.0,
        # )
        # self.bottom = Mountain(
        #     world=self,
        #     x=30,
        #     y=30,
        #     width=10,
        #     height=1.0,
        # )
        self.left_block = Mountain(
            world=self,
            x=-5,
            y=0,
            width=9.5,
            height=10,
        )
        self.right_block = Mountain(
            world=self,
            x=5,
            y=0,
            width=9.5,
            height=10,
        )


        # self.carnivore = Carnivore(self, 25, 25)
        # self.grazer = Grazer(self, 35, 25)
        self.player = Player(self, 0, 0, control=True)
        # self.plant = Plant(self, 20, 25)
