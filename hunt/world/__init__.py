from Box2D import b2World
from hunt.contact_listener import ContactListener
from hunt.actor.mountain import Mountain
from os import makedirs
from random import uniform
from math import ceil
import pprint


class World():
    def __init__(self, game, width=50.0, height=50.0):
        self.game = game
        self.b2world = b2World(
            gravity=(0, 0),
            contactListener=ContactListener()
        )
        self.time_step = 1.0 / 60
        self.velocity_iterations = 10
        self.position_iterations = 10
        self.actors = []

        self.timer = 200
        self.roles = {}

        self.width = width
        self.height = height

        self.left_edge = -1 * (self.width / 2)
        self.right_edge = (self.width / 2)
        self.top_edge = -1 * (self.height / 2)
        self.bottom_edge = (self.height / 2)

        self.left_boundary = self.left_edge * 0.75
        self.right_boundary = self.right_edge * 0.75
        self.top_boundary = self.top_edge * 0.75
        self.bottom_boundary = self.bottom_edge * 0.75

        self.left = Mountain(
            world=self,
            x=self.left_edge,
            y=0.0,
            width=1.0,
            height=self.height,
        )
        self.right = Mountain(
            world=self,
            x=self.right_edge,
            y=0.0,
            width=1.0,
            height=self.height,
        )
        self.top = Mountain(
            world=self,
            x=0.0,
            y=self.top_edge,
            width=self.width,
            height=1.0,
        )
        self.bottom = Mountain(
            world=self,
            x=0.0,
            y=self.bottom_edge,
            width=self.width,
            height=1.0,
        )

        self.colors = {}

    def add_actor(
        self,
        actor_class,
        magnitude=1,
        size=False,
    ):
        for number in range(0, magnitude):
            x, y = self.new_point()
            if size:
                actor = actor_class(self, x, y, size=size)
            else:
                actor = actor_class(self, x, y)

        if magnitude == 1:
            return actor

    def child_close(self):
        pass

    def child_log(self):
        pass

    def child_update(self):
        pass

    def close(self):
        self.child_close()

    def get_player_point(self):
        return self.player.center

    def log(self, role=None):
        self.child_log(role)
        
    def log_attribute(self, role, *args):
        for arg in args:
            number = 1
            self.table[0].append(arg)
            for actor in self.actors:
                value = getattr(actor, arg, 'none')
                if role:
                    if actor.role == role:
                        self.table[number].append(value)
                        number += 1
                else:
                    self.table[number].append(value)
                    number += 1

    def new_point(self, safe=True):
        if safe:
            x = uniform(self.left_boundary, self.right_boundary)
            y = uniform(self.top_boundary, self.bottom_boundary)
        else:
            x = uniform(self.left_edge, self.right_edge)
            y = uniform(self.top_edge, self.bottom_edge)
        return x, y

    def new_color(self):
        red = ceil(uniform(0, 255))
        green = ceil(uniform(0, 255))
        blue = ceil(uniform(0, 255))
        return red, green, blue

    def unique_color(self):
        color = self.new_color()
        while(
            color in self.colors or
            color.count(color[0]) == len(color) or
            (
                color[1] > color[0] + 100 and
                color[1] > color[2] + 100
            ) or
            sum(color) < 100
        ):
            color = self.new_color()
        return color

    def update(self):
        self.b2world.Step(
            self.time_step,
            self.velocity_iterations,
            self.position_iterations
        )
        self.b2world.ClearForces()
        self.child_update()
        for actor in self.actors:
            actor.update()
