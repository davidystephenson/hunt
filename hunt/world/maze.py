from csv import writer
from datetime import datetime
from hunt.actor.mountain import Mountain
from hunt.actor.organism.food.livestock import Livestock
from hunt.actor.organism.food.plant import Plant
from hunt.actor.organism.predator import Predator
from hunt.actor.scenery.river import River
from hunt.actor.scenery.rock import Rock
from hunt.world import World
from math import ceil
from os import makedirs
from os.path import exists, expanduser, join
from random import random, uniform


class Maze(World):
    def __init__(self, game, width=100, height=100):
        World.__init__(self, game, width, height)

        fill = 0.0
        cramp = 0.0

        while cramp < 0.35:
            actor = random()
            x, y = self.new_point(safe=False)
            width = uniform(0.1, 10)
            height = uniform(0.1, 10)


            if actor < 0.5:
                actor = Mountain(self, x, y, width, height)
                fill += width * height
            elif actor < 0.95:
                density = random() 
                actor = Rock(self, x, y, width, height, density=density)
                fill += width * height
            else:
                actor = River(self, x, y, width, height)
                fill += (width * height) / 2

            cramp = fill / (self.width * self.height)

        tenth = self.width / 10
        print "tenth test: " + str(tenth)
        livestock_count = int(tenth)
        print "livestock_count test: " + str(livestock_count)
        self.add_actor(Livestock, magnitude=livestock_count)

        plant_count = livestock_count * 2
        print "plant_count test: " + str(plant_count)
        self.add_actor(Plant, magnitude=plant_count)

        self.intervals = 0
        self.interval = 1.0 / self.width * 15000
        self.intervals = 0
        print "self.interval test:" + str(self.interval)

        x, y = self.new_point()
        self.player = Predator(
            self,
            x,
            y,
            self.unique_color(),
            control=True
        )

        number = ceil(livestock_count / 1.0)
        print "number test:" + str(number)
        color_count = int(number)
        print "color_count test: " + str(color_count)
        for number in range(0, color_count):
            self.unleash()

        self.pvp = False

        self.player.describe()

    def child_log(self, role):
        self.table = []
        self.table.append(
            ['color', 'x', 'y', 'force', 'hunger', 'size']
        )
        if role:
            for color in self.colors:
                x = 0
                y = 0
                force = 0
                hunger = 0
                size = 0

                actors = self.colors[color]
                length = len(actors)

                for actor in actors:
                    if actor.role == role:
                        actor_x = actor.center[0]
                        actor_y = actor.center[1]

                        x = x + actor_x
                        y = y + actor_y
                        force = force + actor.force
                        hunger = hunger + actor.hunger
                        size = size + actor.size

                        self.table.append([
                            actor.color,
                            actor_x,
                            actor_y,
                            actor.force,
                            actor.hunger,
                            actor.size
                        ])

                if length:
                    self.table.append([
                        "Average",
                        x / length,
                        y / length,
                        force / length,
                        hunger / length,
                        size / length
                    ])
        else:
            for actor in self.actors:
                self.table.append([actor])

        directory = join(expanduser('~'), '.predator')
        if not exists(directory):
            makedirs(directory)

        self.today = datetime.now()
        timestamp = self.today.strftime('%Y-%m-%d-%H%M%S')
        filename =  timestamp + '.csv'
        filepath = join(directory, filename)

        with open(filepath, 'w') as file:
            csv_writer = writer(file, dialect='excel')
            for row in self.table:
                csv_writer.writerow(row)

        print('\nLog saved as ' + filepath)

    def child_update(self):
        self.timer += 1

        if not self.pvp:
            self.game.screen.fill((50, 50, 50))

        if self.timer > self.interval:
            plant_size = random() + 0.25
            self.add_actor(Plant, magnitude=3, size=plant_size)

            livestock_size = 3 - plant_size + random()
            self.add_actor(
                Livestock,
                magnitude=1,
                size=livestock_size
            )
            self.timer = 0
            self.intervals += 1

            if self.intervals > 1:
                self.pvp = True

    def child_close(self):
        self.log(role='predator')

    def unleash(self):
        # print "\nUnleash!"
        x, y = self.new_point()
        color = self.unique_color()
        Predator(self, x, y, color)
