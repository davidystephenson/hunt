import pygame
# from hunt.world.test import Test
from hunt.world.maze import Maze

class Game():
    def __init__(self):
        pygame.init()

        self.running = True
        fraction = 1

        self.width = (1920 * 2) / fraction
        self.height = (1080 * 2) / fraction
        self.draw_scale = 75 / fraction

        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((self.width, self.height))
        # self.world = Test(game=self)
        self.world = Maze(game=self)

        self.running = True
        while self.running:
            self.clock.tick(50)
            self.update()

    def close(self):
        self.world.close()
        print('Goodbye!')

    def handle_events(self):
        self.events = pygame.event.get()
        for event in self.events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    self.world.player.die(True)
                if event.key == pygame.K_l:
                    self.world.log()
                if event.key == pygame.K_t:
                    self.world.player.reproduce()
                    self.world.player.health = 0.01

            if event.type == pygame.QUIT:
                self.running = False

    def update(self):
        self.handle_events()
        self.screen.fill((0, 0, 0))
        self.world.update()
        pygame.display.flip()
