from Box2D import b2_dynamicBody, b2_staticBody


class Body():
    def __init__(
        self,
        actor,
        x=0,
        y=0,
        body_type='dynamic'
    ):
        self.actor = actor
        self.world = actor.world
        self.game = self.world.game
        self.fixtures = []
        if body_type == 'dynamic':
            self.body_type = b2_dynamicBody
        else:
            self.body_type = b2_staticBody
        self.b2body = self.world.b2world.CreateBody(
            type=self.body_type,
            position=(x, y),
            userData=self,
            linearDamping=0.5,
        )
        self.actor.bodies.append(self)

    def draw(self):
        for fixture in self.fixtures:
            fixture.draw()
