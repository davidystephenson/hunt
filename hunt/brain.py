from math import sqrt
from random import uniform


class Brain():
    def __init__(
        self,
        world,
        predator,
    ):
        self.world = world
        self.predator = predator
        self.game = self.world.game
        self.body = self.predator.body

    def distance_to(self, actor):
        other_x, other_y = actor.center
        my_x, my_y = self.predator.center
        dx = my_x - other_x
        dy = my_y - other_y
        return sqrt(dx ** 2 + dy ** 2)

    def nearest(self):
        actors = []
        for actor in self.world.actors:
            color = actor.color
            if(
                color.count(color[0]) != len(color) and
                color != (255, 255, 0) and
                color != self.predator.color
            ):
                actors.append(actor)

        distances = [self.distance_to(actor) for actor in actors]
        sorted_actors = []
        for (distance, actor) in sorted(zip(distances, actors)):
            close_x, close_y = actor.center
            my_x, my_y = self.predator.center

            x_distance = abs(my_x - close_x) * self.game.draw_scale
            y_distance = abs(my_y - close_y) * self.game.draw_scale

            if(
                x_distance < self.game.width * 2 and
                y_distance < self.game.height * 2
            ):
                sorted_actors.append(actor)
        return sorted_actors

    def rush(self, target, force):
        target_x, target_y = target.center
        self_x, self_y = self.predator.center
        if self_y > target_y:
            self.body.b2body.ApplyForce(
                force=(0, -force),
                point=self.predator.center,
                wake=True,
            )
        else:
            self.body.b2body.ApplyForce(
                force=(0, force),
                point=self.predator.center,
                wake=True,
            )

        if self_x > target_x:
            self.body.b2body.ApplyForce(
                force=(-force, 0),
                point=self.predator.center,
                wake=True,
            )
        else:
            self.body.b2body.ApplyForce(
                force=(force, 0),
                point=self.predator.center,
                wake=True,
            )

    def search(self):
        x_force = uniform(-self.force, self.force)
        y_force = uniform(-self.force, self.force)
        self.body.b2body.ApplyForce(
            force=(x_force, y_force),
            point=self.predator.center,
            wake=True,
        )
