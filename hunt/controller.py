import pygame

class Controller():
    def __init__(self, predator):
        self.predator = predator

    def control(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP] or keys[pygame.K_w]:
            self.predator.body.b2body.ApplyForce(
                force=(0, -self.predator.force),
                point=self.predator.center,
                wake=True,
            )
        if keys[pygame.K_DOWN] or keys[pygame.K_s]:
            self.predator.body.b2body.ApplyForce(
                force=(0, self.predator.force),
                point=self.predator.center,
                wake=True,
            )
        if keys[pygame.K_LEFT] or keys[pygame.K_a]:
            self.predator.body.b2body.ApplyForce(
                force=(-self.predator.force, 0),
                point=self.predator.center,
                wake=True,
            )
        if keys[pygame.K_RIGHT] or keys[pygame.K_d]:
            self.predator.body.b2body.ApplyForce(
                force=(self.predator.force, 0),
                point=self.predator.center,
                wake=True,
            )
