from hunt.fixture import Fixture
from pygame import draw


class Triangle(Fixture):
    def __init__(
        self,
        body,
        color=(255, 255, 255),
        size=None,
        x=0,
        y=0,
        width=1.0,
        height=1.0,
        density=1.0,
        is_sensor=False,
    ):
        Fixture.__init__(self, body, color, size)
        self.width = width
        self.height = height
        if self.size:
            self.width = self.size
            self.height = self.size

        self.points = [
            (x - self.width / 2, y - self.height / 2),
            (x + self.width / 2, y - self.height / 2),
            (x, y + height / 2),
        ]

        self.b2fixture = self.b2body.CreatePolygonFixture(
            vertices=self.points,
            density=density,
            friction=self.friction,
            restitution=self.restitution,
            userData=self,
        )
        self.b2fixture.sensor = is_sensor

    def draw(self):
        screen_points = [
            self.get_screen_point(point) for point in self.points
        ]
        draw.polygon(
            self.game.screen,
            self.color,
            screen_points,
        )
