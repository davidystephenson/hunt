class Fixture():
    def __init__(self, body, color, size=None):
        self.body = body
        self.color = color
        self.size = size
        self.actor = body.actor
        self.world = self.actor.world
        self.game = self.world.game
        self.b2body = body.b2body
        self.friction = 0.4
        self.restitution = 0.5
        self.body.fixtures.append(self)

    def get_screen_point(self, point):
        draw_scale = self.game.draw_scale
        world_point = self.get_world_point(point)
        player_point = self.world.get_player_point()
        screen_point = (
            (
                (world_point[0] - player_point[0]) *
                draw_scale + self.game.width / 2
            ),
            (
                (world_point[1] - player_point[1]) *
                draw_scale + self.game.height / 2
            ),
        )
        return screen_point

    def get_world_point(self, point):
        return self.body.b2body.GetWorldPoint(point)

    def draw(self):
        pass
