from hunt.fixture import Fixture
from pygame import draw


class Circle(Fixture):
    def __init__(
            self,
            body,
            color=(255, 255, 255),
            size=None,
            x=0,
            y=0,
            is_sensor=False,
            density=1.0
    ):
        Fixture.__init__(self, body, color, size)
        self.x = x
        self.y = y
        self.b2fixture = self.b2body.CreateCircleFixture(
            radius=self.size,
            pos=(self.x, self.y),
            density=density,
            friction=self.friction,
            restitution=self.restitution,
            userData=self,
        )
        self.b2fixture.sensor = is_sensor

    def draw(self):
        center = self.get_screen_point((self.x, self.y))
        int_center = (int(center[0]), int(center[1]))
        draw_scale = self.game.draw_scale
        int_radius = int(self.size * draw_scale)
        #print "self.color test: " + str(self.color)
        try:
            draw.circle(
                self.game.screen,
                self.color,
                int_center,
                int_radius
            )
        except TypeError:
            print "Fixture error: " + str(self.color)
